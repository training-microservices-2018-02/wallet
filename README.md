# Aplikasi Wallet Service #

## Setup Database ##

* Login ke database sebagai root

        $ mysql -u root
        Welcome to the MySQL monitor.  Commands end with ; or \g.
        Your MySQL connection id is 2
        Server version: 5.7.23 Homebrew

        Copyright (c) 2000, 2018, Oracle and/or its affiliates. All rights reserved.

        Oracle is a registered trademark of Oracle Corporation and/or its
        affiliates. Other names may be trademarks of their respective
        owners.

        Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

        mysql>

* Buat database

        create database walletdb;

* Buat user untuk mengakses database

        grant all on walletdb.* to wallet@localhost identified by 'wallet123';

## Jalankan Aplikasi ##

```
mvn spring-boot:run
```

Browse ke http://localhost:10001

## Deploy ke Heroku ##

1. Buat aplikasi di Heroku

        heroku apps:create tms-wallet
        heroku apps:create tms-wallet-1
        heroku apps:create tms-wallet-2

2. Set environment variable

        heroku config:set SPRING_PROFILES_ACTIVE=heroku -a tms-wallet
        heroku config:set SPRING_PROFILES_ACTIVE=heroku -a tms-wallet-1
        heroku config:set SPRING_PROFILES_ACTIVE=heroku -a tms-wallet-2

3. Enable dyno info supaya variabel `HEROKU_APP_NAME` terisi

        heroku labs:enable runtime-dyno-metadata -a tms-wallet
        heroku labs:enable runtime-dyno-metadata -a tms-wallet-1
        heroku labs:enable runtime-dyno-metadata -a tms-wallet-2

4. Buat database di aplikasi pertama

        heroku addons:create heroku-postgresql:hobby-dev -a tms-wallet

    Outputnya seperti ini:

        Creating heroku-postgresql:hobby-dev on ⬢ tms-wallet... free
        Database has been created and is available
         ! This database is empty. If upgrading, you can transfer
         ! data from another database with pg:copy
        Created postgresql-infinite-39847 as DATABASE_URL
        Use heroku addons:docs heroku-postgresql to view documentation

5. Sambungkan database di aplikasi pertama ke replikanya

        heroku addons:attach postgresql-infinite-39847 -a tms-wallet-1
        heroku addons:attach postgresql-infinite-39847 -a tms-wallet-2

6. Deploy dengan Gitlab-CI