package com.artivisi.training.microservice.wallet.service;

import com.artivisi.training.microservice.wallet.dto.Notification;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaService {
    
    @Value("${kafka.topic.notification}") private String topicNotification;
    
    @Autowired private KafkaTemplate kafkaTemplate;
    @Autowired private ObjectMapper objectMapper;
    
    public void sendNotification(String from, String to, String subject, String body){
        try {
            Notification notif = new Notification();
            notif.setFrom(from);
            notif.setTo(to);
            notif.setSubject(subject);
            notif.setBody(body);
            
            kafkaTemplate.send(topicNotification, objectMapper.writeValueAsString(notif));
        } catch (JsonProcessingException ex) {
            Logger.getLogger(KafkaService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
