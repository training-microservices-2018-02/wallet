package com.artivisi.training.microservice.wallet.dao;

import com.artivisi.training.microservice.wallet.entity.Wallet;
import com.artivisi.training.microservice.wallet.entity.WalletTransaction;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface WalletTransactionDao extends PagingAndSortingRepository<WalletTransaction, String>{

    public Iterable<WalletTransaction> findByWalletOrderByTransactionTime(Wallet wallet);
    
}
