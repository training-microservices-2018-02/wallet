package com.artivisi.training.microservice.wallet.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HostInfoController {
    @GetMapping("/host/info")
    public Map<String, String> info(HttpServletRequest req){
        Map<String, String> hasil = new HashMap<>();
        hasil.put("Local IP", req.getLocalAddr());
        hasil.put("Local Port", String.valueOf(req.getLocalPort()));
        hasil.put("Hostname", req.getLocalName());
        
        /*
        try {
            Thread.sleep(5 * 1000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        */
        
        return hasil;
    }
}