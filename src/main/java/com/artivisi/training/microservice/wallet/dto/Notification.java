package com.artivisi.training.microservice.wallet.dto;

import lombok.Data;

@Data
public class Notification {
    private String from;
    private String to;
    private String subject;
    private String body;
}
