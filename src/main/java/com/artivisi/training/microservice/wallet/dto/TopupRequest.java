package com.artivisi.training.microservice.wallet.dto;

import java.math.BigDecimal;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class TopupRequest {
    
    @NotNull @Min(10000)
    private BigDecimal amount;
    private String description;
}
