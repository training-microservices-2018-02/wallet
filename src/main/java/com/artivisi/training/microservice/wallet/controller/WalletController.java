package com.artivisi.training.microservice.wallet.controller;

import com.artivisi.training.microservice.wallet.dao.WalletDao;
import com.artivisi.training.microservice.wallet.dao.WalletTransactionDao;
import com.artivisi.training.microservice.wallet.dto.TopupRequest;
import com.artivisi.training.microservice.wallet.entity.TransactionType;
import com.artivisi.training.microservice.wallet.entity.Wallet;
import com.artivisi.training.microservice.wallet.entity.WalletTransaction;
import com.artivisi.training.microservice.wallet.service.KafkaService;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController @Transactional
public class WalletController {
    @Autowired private WalletDao walletDao;
    @Autowired private WalletTransactionDao walletTransactionDao;
    @Autowired private KafkaService kafkaService;

    @GetMapping("/wallet/")
    public Page<Wallet> semuaWallet(Pageable page){
        return walletDao.findAll(page);
    }
    
    @GetMapping("/wallet/{wallet}/transaction")
    public Iterable<WalletTransaction> listTopup(@PathVariable Wallet wallet){
        return walletTransactionDao.findByWalletOrderByTransactionTime(wallet);
    }
    
    @PostMapping("/wallet/{wallet}/topup")
    @ResponseStatus(HttpStatus.CREATED)
    public void insertTopup(@PathVariable Wallet wallet, @RequestBody @Valid TopupRequest topup){
        WalletTransaction wt = new WalletTransaction();
        wt.setWallet(wallet);
        wt.setTransactionType(TransactionType.TOPUP);
        wt.setDescription(topup.getDescription());
        wt.setAmount(topup.getAmount());
        walletTransactionDao.save(wt);
        
        wallet.setBalance(wallet.getBalance().add(wt.getAmount()));
        walletDao.save(wallet);
        
        kafkaService.sendNotification("Aplikasi Wallet", wallet.getOwner().getEmail(), "Topup Success", 
                "Topup : "+wt.getDescription()
                        + " Rp. "+wt.getAmount()
                        +" success. Current balance : "+wallet.getBalance().toString());
    }

}