package com.artivisi.training.microservice.wallet.dao;

import com.artivisi.training.microservice.wallet.entity.Wallet;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface WalletDao extends PagingAndSortingRepository<Wallet, String> {

}